import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.HashMap;

/**
 * BillPrint - adapted from Refactoring, 2nd Edition by Martin Fowler
 *
 * @author Minglun Zhang
 */
public class BillPrint {
    private HashMap<String, Play> plays;
    private String customer;
    private ArrayList<Performance> performances;

    public BillPrint(ArrayList<Play> plays, String customer, ArrayList<Performance> performances) {
        this.plays = new HashMap();
        for (Play p : plays) {
            this.plays.put(p.getId(), p);
        }

        this.customer = customer;
        this.performances = performances;
    }

    public HashMap<String, Play> getPlays() {
        return plays;
    }


    public String getCustomer() {
        return customer;
    }

    public ArrayList<Performance> getPerformances() {
        return performances;
    }


    public int amountFor(Performance perf) {
        //int thisAmount = 0;
        int output = 0;
        //switch (play.getType()) {
        switch (playFor(perf).getType()) {
            case "tragedy":
                output = 40000;
                if (perf.getAudience() > 30) {
                    output += 1000 * (perf.getAudience() - 30);
                }
                break;
            case "comedy":
                output = 30000;
                if (perf.getAudience() > 20) {
                    output += 10000 + 500 * (perf.getAudience() - 20);
                }
                output += 300 * perf.getAudience();
                break;
            //default:
            //  throw new IllegalArgumentException("unknown type: " + play.getType());
            default:
                throw new IllegalArgumentException("unknown type: " + playFor(perf).getType());
        }
        return output;
    }

    public Play playFor(Performance perf) {
        Play play = plays.get(perf.getPlayID());
        if (play == null) {
            throw new IllegalArgumentException("No play found");
        }
        return play;
    }

    public int volumeCreditsFor(Performance perf){
       // int volumeCredits = 0;
        //volumeCredits += Math.max(perf.getAudience() - 30, 0);
        int result = 0;
        result += Math.max(perf.getAudience() - 30, 0);

        if ( playFor(perf).getType().equals("comedy")) {
            //volumeCredits += Math.floor((double) perf.getAudience() / 5.0);
            result += Math.floor((double) perf.getAudience() / 5.0);
        }
      //  return volumeCredits;
        return result;
    }
    public int totalVolumeCredits(){
        //int volumeCredits = 0;
        int result =0;
        for (Performance perf: performances){
           // volumeCredits += volumeCreditsFor(perf);
            result += volumeCreditsFor(perf);
        }
       // return volumeCredits;
        return result;
    }

    public String usd(double number) {
        DecimalFormat numberFormat = new DecimalFormat("#.00");
        //return numberFormat.format(number);
        return "$" + numberFormat.format(number/100);

    }
   // public int appleSauce() {
     //   int totalAmount = 0;
        //int volumeCredits = 0;
   public int totalAmount(){
       int result = 0;

       //String result = "Statement for " + this.customer + "\n";
        for (Performance perf : performances) {
            result += amountFor(perf);
        }
        return result;
    }
    public String statement() {
        //int totalAmount = appleSauce();
        for (Performance perf : performances) {
          /*  Play play = plays.get(perf.getPlayID());
            if (play == null) {
                throw new IllegalArgumentException("No play found");
            }

			int thisAmount = 0;

			switch (play.getType()) {
				case "tragedy": thisAmount = 40000;
					            if (perf.getAudience() > 30) {
						             thisAmount += 1000 * (perf.getAudience() - 30);
					            }
								break;
				case "comedy":  thisAmount = 30000;
					            if (perf.getAudience() > 20) {
						            thisAmount += 10000 + 500 * (perf.getAudience() - 20);
					            }
					            thisAmount += 300 * perf.getAudience();
								break;
				default:        throw new IllegalArgumentException("unknown type: " +  play.getType());
			}
*/
            // add volume credits
//Play play =playFor(perf);
            // int thisAmount = amountFor(perf, play);
          //  int thisAmount = amountFor(perf);
           /* volumeCredits += Math.max(perf.getAudience() - 30, 0);
            //if (play.getType().equals("comedy")) {
            if (playFor(perf).getType().equals("comedy")) {
                volumeCredits += Math.floor((double) perf.getAudience() / 5.0);
            }
*/
            // print line for this order
            // result += "  " + play.getName() + ": $" + numberFormat.format((double) thisAmount / 100.00) + " (" + perf.getAudience()
            //result += "  " + playFor(perf).getName() + ": $" + numberFormat.format((double) thisAmount / 100.00) + " (" + perf.getAudience()
            //volumeCredits += volumeCreditsFor(perf);
            result += "  " + playFor(perf).getName() + ": " + usd((double) amountFor(perf) / 100.00) + " (" + perf.getAudience()
                    + " seats)" + "\n";
            //totalAmount += amountFor(perf);
        }
       /* int volumeCredits = 0;
        for (Performance perf: performances){
            volumeCredits += volumeCreditsFor(perf);
        }
*/
        //int volumeCredits = totalVolumeCredits();
        //result += "Amount owed is " + usd((double) totalAmount ) + "\n";
        result += "Amount owed is " + usd(totalAmount()) + "\n";
        result += "You earned " + totalVolumeCredits() + " credits" + "\n";
        return result;
    }

    public static void main(String[] args) {
        Play p1 = new Play("hamlet", "Hamlet", "tragedy");
        Play p2 = new Play("as-like", "As You Like It", "comedy");
        Play p3 = new Play("othello", "Othello", "tragedy");
        ArrayList<Play> pList = new ArrayList<Play>();
        pList.add(p1);
        pList.add(p2);
        pList.add(p3);
        Performance per1 = new Performance("hamlet", 55);
        Performance per2 = new Performance("as-like", 35);
        Performance per3 = new Performance("othello", 40);
        ArrayList<Performance> perList = new ArrayList<Performance>();
        perList.add(per1);
        perList.add(per2);
        perList.add(per3);
        String customer = "BigCo";
        BillPrint app = new BillPrint(pList, customer, perList);
        System.out.println(app.statement());
    }

}
